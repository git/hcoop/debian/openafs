General Maintenance

    This package is maintained in Git via the Salsa debian project.
    Salsa is used only for repository access control and not for any of
    its other features.

    Since we often pull up many upstream fixes from the upstream stable
    branch due to slow upstream release frequencies, we use Git to handle
    merging and patch pullups and do not attempt to export the Git
    repository state as a patch set.  This package uses 3.0 (quilt) via
    the gbp pq subcommands.

    Ideally, any changes that are not strictly Debian packaging changes
    should be submitted upstream first.  Upstream uses Gerrit for patch
    review, which makes it very easy for anyone who wishes to submit
    patches for review using Git.  See:

        https://wiki.openafs.org/devel/GitDevelopers/

    for information on how to submit patches upstream.  Starting from
    OpenAFS 1.5, we're no longer carrying any substantial Debian-specific
    changes outside of the debian/* directory, only temporary bug
    workarounds, and we want to keep it that way.

Importing a New Upstream Release

    We want to be able to use Git to cherry-pick fixes from upstream, but
    we want to base the Debian packages on the upstream tarball releases.
    We also need to strip some non-DFSG files from the upstream tarball
    releases and imported code, and want to drop the WINNT directory to
    save some space.  This means we follow a slightly complicated method
    for importing a new upstream release.

    Follow the following procedure to import a new upstream release:

    1. Update the package version in debian/changelog to match the new
       upstream version.  If the new upstream version is a prerelease
       version, don't forget to add "~" before "pre" so that the versions
       will sort property.

    2. Double-check the TAG setting in debian/rules to be sure it's going
       to retrieve the correct Git tag.

    3. Run debian/rules get-orig-source.  This will generate a tarball
       from the upstream Git tag using git archive, remove the WINNT
       directory, and create a file named openafs_<version>.orig.tar.xz in
       the current directory.

    4. Ensure that you have the OpenAFS upstream Git repository available
       as a remote in the Git repository where you're doing the packaging
       work and it's up to date:

           git remote add openafs git://git.openafs.org/openafs.git
           git fetch openafs

       This will be required to locate the tag for the new upstream
       release.

    5. Determine the release tag corresponding to this tarball.  At the
       time of this writing, upstream uses tags in the form:

           openafs-stable-<version>
           openafs-devel-<version>

       for stable and development releases respectively.  <version> is the
       version number with periods replaced by underscores.  This
       convention may change, so double-check with git tag.

    6. Import the upstream source from the tarball with:

           gbp import-orig --upstream-vcs-tag <tag> <tarball>

       where <tarball> is the tarball created by get-orig-source above and
       <tag> is the corresponding tag from the upstream Git repository.

    7. Flesh out the changelog entry for the new version with a summary of
       what changed in that release, and continue as normal with Debian
       packaging.

Pulling Upstream Changes

    Upstream releases, particularly stable releases, are relatively
    infrequent, so it's often desirable to pull upstream changes from the
    stable branch into the Debian package.  This should always be done
    using git cherry-pick -x so that we can use git cherry to see which
    changes on the stable branch have not been picked up.

    The procedure is therefore:

    0. Regenerate and switch to the patch-queue branch with
       git branch -d patch-queue/master && gbp pq import

    1. Identify the hash of the commit that you want to pull up using git
       log or other information.

    2. git cherry-pick -x <hash>.  If the cherry-pick fails and you have
       to manually do a merge, follow the instructions to use -c to keep
       the original commit message as a starting point, but *also*
       manually add a line like:

           (cherry picked from commit <hash>)

       to the changelog entry where <hash> is the full hash of the
       upstream commit.  Note that the upstream commits on the stable
       branch will generally already have a line like this from upstream's
       cherry-pick.  This will be a second line.

    3. Switch to the master branch and (re)generate patch files:
       git checkout master && gbp pq export

    4. Add a changelog entry and commit it along with the added patch files.
       Use the following convention for changelog entries for cherry-picks:

           * Apply upstream deltas:
             - [<hash>] <title>
             - ...

       where <hash> is the first eight characters of the upstream commit
       hash and <title> is the first line of the upstream commit message,
       edited as necessary to keep the length of the changelog lines
       down.

 -- Russ Allbery <rra@debian.org>, Sun, 20 Oct 2013 08:59:17 -0700
 -- Benjamin Kaduk <kaduk@mit.edu>, Mon 22 Sep 2014 13:05:40 -0400
